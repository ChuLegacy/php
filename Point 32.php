<?php

//En programación orientada a objetos, el polimorfismo o poliformismo se refiere a la propiedad
// por la que es posible enviar mensajes sintácticamente iguales a objetos de tipos distintos.
// El único requisito que deben cumplir los objetos que se utilizan de manera polimórfica es saber
// responder al mensaje que se les envía.
//
//La apariencia del código puede ser muy diferente dependiendo del lenguaje que se utilice,
//más allá de las obvias diferencias sintácticas.
//
//Por ejemplo, en un lenguaje de programación que cuenta con un sistema de tipos dinámico
//(en los que las variables pueden contener datos de cualquier tipo u objetos de cualquier clase)
//como Smalltalk se requiere que los objetos que se utilizan de modo polimórfico sean parte de una
//jerarquía de clases.
