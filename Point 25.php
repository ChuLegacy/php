<?php
class ClaseSencilla {
  // Válido a partir de PHP 5.6.0:
  public $var1 = 'hola ' . 'mundo';
  // Válido a partir de PHP 5.3.0:
  public $var2 = <<<EOD
hola mundo
EOD;
  // Válido a partir de PHP 5.6.0:
  public $var3 = 1+2;
  // Declaraciones de propiedades inválidas:
  public $var4 = self::miMétodoEstático();
  public $var5 = $myVar;

  // Declaraciones de propiedades válidas:
  public $var6 = miConstante;
  public $var7 = array(true, false);

  // Válido a partir de PHP 5.3.0:
  public $var8 = <<<'EOD'
hola mundo
EOD;
}
