<?php
//ejemplo de sintaxis de espacios de nombres

namespace mi\nombre; // véase la sección "Definir espacios de nombres"

class MiClase {

}

function mifunción() {

}

const MICONSTANTE = 1;

$a = new MiClase;
$c = new \mi\nombre\MiClase; // véase la sección "Espacio global"

$a = strlen('hola'); // véase la sección "Utilizar espacios de nombres: una
// alternativa a funciones/constantes globales"

$d = namespace\MICONSTANTE; // véase la sección "El operador namespace y
// la constante __NAMESPACE__"
$d = __NAMESPACE__ . '\MICONSTANTE';
echo constant($d); // véase la sección "Espacios de nombres y características dinámicas del lenguaje"
