<?php
//Cuando se trabaja con una aplicación, se abre, hace algunos cambios, y luego lo cierra. Esto es muy parecido a una sesión. El equipo sabe quién eres. Se sabe cuando se inicia la aplicación y cuando se termina. Sin embargo, en Internet hay un problema: el servidor web no sabe quién es ni lo que haces, porque la dirección HTTP no mantiene estado.
//
//Las variables de sesión resolver este problema mediante el almacenamiento de la información
// del usuario a utilizar en varias páginas (por ejemplo, nombre de usuario, color favorito, etc.).
//  Por defecto, las variables de sesión duran hasta que el usuario cierra el navegador.
//
//Asi que; Las variables de sesión contienen información acerca de un solo usuario,
// y están disponibles para todas las páginas en una sola aplicación.
//
//
//Una sesión se inicia con la función session_start ().
//Las variables de sesión se establecen con la variable global PHP: $ _SESSION.
//Ahora, vamos a crear una nueva página llamada "demo_session1.php".
// En esta página, se comienza una nueva sesión de PHP y establecer algunas variables de sesión:
//
//<?php
//// Start the session
//session_start();
//
?>
<!DOCTYPE html>
<html>
  <body>

    //<?php
//// Set session variables
//$_SESSION["favcolor"] = "green";
//$_SESSION["favanimal"] = "cat";
//echo "Session variables are set.";
//

//</body>
//</html>
