//sort () - ordenar matrices de clasificación en orden ascendente
//rsort () - ordenar matrices de clasificación en orden descendente
//asort () - ordenar matrices asociativas en orden ascendente, de acuerdo con el valor
//ksort () - ordenar matrices asociativas en orden ascendente, de acuerdo con la clave
//arsort () - ordenar matrices asociativas en orden descendente, de acuerdo con el valor
//krsort () - ordenar matrices asociativas en orden descendente, de acuerdo con la clave

<?php
$cars = array("Volvo", "BMW", "Toyota");
sort($cars);

$cars2 = array("Volvo", "BMW", "Toyota");
rsort($cars2);

$age = array("Peter" => "35", "Ben" => "37", "Joe" => "43");
asort($age);

$age2 = array("Peter" => "35", "Ben" => "37", "Joe" => "43");
ksort($age2);

$age3 = array("Peter" => "35", "Ben" => "37", "Joe" => "43");
arsort($age3);

$age4 = array("Peter" => "35", "Ben" => "37", "Joe" => "43");
krsort($age4);
