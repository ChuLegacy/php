// matriz asociativa
//Las matrices asociativas son matrices que utilizan nombres de clave que asigna a ellos.
//Hay dos maneras de crear una matriz asociativa:

<?php
$age1 = array("Peter" => "35", "Ben" => "37", "Joe" => "43");
//o
$age2['Peter'] = "35";
$age2['Ben'] = "37";
$age2['Joe'] = "43";
//ejemplo :
$age3 = array("Peter" => "35", "Ben" => "37", "Joe" => "43");
echo "Peter is " . $age3['Peter'] . " years old.";
