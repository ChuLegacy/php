//funcion sin argumento y sin return :
<?php
function writeMsg() {
  echo "Hello world!";
}

writeMsg(); // llamar la funcion
//
// con argumento y sin return :
//La información puede suministrarse a las funciones a través de argumentos. Un argumento es como una variable.
//Los argumentos se especifican después de que el nombre de la función,
// dentro de los paréntesis.
// Puede añadir tantos argumentos como desee, sólo les separan con una coma.

function familyName($fname) {
  echo "$fname Refsnes.<br>";
}

familyName("Jani");
familyName("Hege");
familyName("Stale");
familyName("Kai Jim");
familyName("Borge");

//sin argumento y con retutn :
//Para permitir que una función devuelve un valor, utilice la instrucción de retorno:
function sum() {
  $z = 1 + 2;
  return $z;
}

echo "1 + 2 = " . sum();

// con argumentos y return :
function sum2($x, $y) {
  $z = $x + $y;
  return $z;
}

echo "5 + 10 = " . sum2(5, 10) . "<br>";
echo "7 + 13 = " . sum2(7, 13) . "<br>";
echo "2 + 4 = " . sum2(2, 4);
