//String, Integer, Float, Boolean, Array, Object, NULL, Resource
//Example

//string
<?php
$x = "Hello world!";
$y = 'Hello world!';

echo $x;
echo "<br>";
echo $y;

//integer
$x = 5985;
var_dump($x);

//Float
$x = 10.365;
var_dump($x);

//Boolean
$x = true;
$y = false;

//Array
$cars = array("Volvo","BMW","Toyota");
var_dump($cars);

//Objet
class Car {
    function Car() {
        $this->model = "VW";
    }
}

// create an object
$herbie = new Car();

// show object properties
echo $herbie->model;

//NULL
$x = "Hello world!";
$x = null;
var_dump($x);