
<?php
//Los operadores PHP aritméticas se utilizan con los valores numéricos para realizar
// operaciones aritméticas comunes, tales como suma, resta, multiplicación, etc.
//
//Operador	Nombre	        Ejemplo
// +	    suma	        $x + $y
// -	        resta	        $x - $y
// *	        Multiplicacion	$x * $y
// /	    Division	    $x / $y
// %	    Modulo	        $x % $y
// **	    Exponenciacion	$x ** $y

//Los operadores de asignación PHP se utilizan con los valores numéricos para escribir un valor a una variable.

//El operador básico de asignación en PHP es "=".
//Esto significa que el operando de la izquierda se establece en el valor de la expresión
// de asignación a la derecha.

//Asgnacion   Igual que...
// x = y	  x = y
// x += y	  x = x + y
// x -= y	  x = x - y
// x *= y	  x = x * y
// x /= y	  x = x / y
// x %= y	  x = x % y

//Los operadores de comparación PHP se utilizan para comparar dos valores (número o cadena):
//Operador	Nombre	                    Ejemplo
// ==	    Igualdad	                $x == $y
// ===	    Idéntico	                $x === $y
// !=	    No igual    	            $x != $y
// <>	    No igual 	                $x <> $y
// !==	    No identico                 $x !== $y
// >	        Mayor que	                $x > $y
// <	        Menor que                 	$x < $y
// >=	    Mayor o igual que	        $x >= $y
// <=       Menor o igual que           $x <= $y


//Los operadores de incremento PHP se utilizan para incrementar el valor de una variable.

//Los operadores de PHP decremento se utilizan para disminuir el valor de una variable.
//Operador 	Nombre
// ++$x     Pre-incremento
// $x++	    Post-incremento
// --$x	    Pre-decremento
// $x--	    Post-decremento

//Los operadores lógicos de PHP se utilizan para combinar las sentencias condicionales.
// Operador	Nombre	Ejemplo
// and	    y	    $x and $y
// or	    O	    $x or $y
// xor	    Xor	    $x xor $y
// &&	    y	    $x && $y
// ||	    O	    $x || $y
// !     	No	    !$x


//PHP tiene dos operadores que están especialmente diseñados para las cadenas.
// Operador	  Nombre	                    Ejemplo
// .	          Concatenacion	                $txt1 . $txt2
// .=	      Asignación de concatenación	$txt1 .= $txt2

//Los operadores de matriz PHP se utilizan para comparar matrices.
// Operador	  Nombre	    Ejemplo
// +	      Union	        $x + $y
// ==	      Igualdad	    $x == $y
// ===        Identidad     $x ==== $y
// ! =        Desigualdad   $x! = $y
// <>	      Desigualdad	$x <> $y
// !==	      No identidad	$x !== $y
