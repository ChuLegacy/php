//Las funciones anónimas
//permiten la creación de funciones que no tienen un nombre especificado.
//Son más útiles como valor de los parámetros de llamadas de retorno, pero tienen muchos otros usos.
<?php
echo preg_replace_callback('~-([a-z])~', function ($coincidencia) {
  return strtoupper($coincidencia[1]);
}, 'hola-mundo');
