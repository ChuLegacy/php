//En la programación, un método es una subrutina cuyo código es definido en una clase
// y puede pertenecer tanto a una clase, como es el caso de los métodos de clase o estáticos,
// como a un objeto, como es el caso de los métodos de instancia.
// Declaración de un método :
//
//public $var = 'un valor predeterminado';
//
//public function mostrarVar() {
//  echo $this->var;
//}

<?php

