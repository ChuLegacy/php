//una matriz tridimensional es una matriz de matrices de matrices :
<?php
$datos = array(
    array(array(0, 0, 0),
        array(0, 0, 1),
        array(0, 0, 2)
    ),
    array(array(0, 1, 0),
        array(0, 1, 1),
        array(0, 1, 2)
    ),
    array(array(0, 2, 0),
        array(0, 2, 1),
        array(0, 2, 2)
    )
);
